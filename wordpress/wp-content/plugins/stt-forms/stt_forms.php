<?php
/**
 * Plugin Name: STT Forms 
 * Plugin URI: https://seventeentech.com
 * Description: Plugin for managing a database of customers
 * Author: Dinu Oltean
 * Version: 1.0
 * Author URI: https://seventeentech.com
 * Text Domain: customers-db
 * Domain Path: /languages
 */
/*
 * 
 */
if ( !defined( 'ABSPATH' ) )
  exit;
@define('STT_FORMS_PATH', WP_PLUGIN_URL . '/' . end(explode(DIRECTORY_SEPARATOR, dirname(__FILE__))));
@define('STT_FORMS_TABLE', 'stt_forms_settings');

if ( ! function_exists( 'is_plugin_active' ) )
    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

//register_forms_scripts();
include_once('includes/enquire_form.php');
include_once('includes/ajax_enquire_form.php');
include_once('includes/forms_setup.php');
include_once('includes/email_stuff.php');

//add_action( ‘plugins_loaded’, array( $this, 'enqueue_forms_scripts' ) );
add_action('wp_print_styles', 'enqueue_forms_styles');
//setup AJAX so we can stay on the same page
add_action( 'wp_ajax_ajax_enquire_form', 'ajax_enquire_form' );
add_action( 'wp_ajax_nopriv_ajax_enquire_form', 'ajax_enquire_form' );
//Shortcodes to pull in the form into any page
register_activation_hook(__FILE__, 'install_stt_forms_table');
add_shortcode('enquire_form', 'enquire_form');
add_action('admin_menu', 'manage_stt_forms');

function install_stt_forms_table(){
    create_settings_table(STT_FORMS_TABLE);
}
function manage_stt_forms() {
    add_menu_page('STT Forms', 'STT Forms', 'manage_options', 'forms_setup');
    add_submenu_page('stt_forms', 'Settings', 'Settings', 'manage_options', 'forms_setup', 'forms_setup');
}
/**
 * Register plugin styles and scripts
 */
//    wp_register_script( 'forms-script', plugins_url( 'js/enquire_form.js', __FILE__ ), array('jquery'), null, true );
//    wp_register_style( 'forms-style', plugins_url( 'css/enquire_form.css' ) );
//} 
/**
 * Enqueues plugin-specific scripts.
 */
//function enqueue_forms_scripts() {        
//    wp_enqueue_script( 'forms-script' );
//}   
/**
 * Enqueues plugin-specific styles.
 */
function enqueue_forms_styles() {   
    
    wp_enqueue_style('enquire_form_css', STT_FORMS_PATH . '/css/enquire_form.css', '', rand(111,9999));    
    //wp_enqueue_style( 'forms-style' ); 
}
//spl_autoload_register( 'CDb_include_files' );


