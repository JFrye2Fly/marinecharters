<?php
/*
function debug_to_console( $data ) {
    $output = $data;
    if ( is_array( $output ) )
        $output = implode( ',', $output);

    echo "<script>console.log( 'Debug Objects: " . $output . "' );</script>";
}
*/
function sendEmail($subject, $message){
    global $wpdb;
    $formsSettings = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . STT_FORMS_TABLE);
    $setData = $formsSettings[0]->settings_data;
    if(strlen(trim($setData)) >0 ){
        $emailData = json_decode($setData, true);
        $to_email = $emailData['to_email'];
        $cc_email = $emailData['cc_email'];
        $bcc_email = $emailData['bcc_email'];
        
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        //$headers .= 'From: ' . get_option( 'blogname' ) . '<' . get_option( 'admin_email' ) . '>' . "\r\n";
        if($cc_email)
            $headers .= 'Cc: '.  $cc_email . "\r\n";
        if($bcc_email)
            $headers .= 'Bcc: '.$bcc_email . "\r\n";
        wp_mail($to_email, $subject, $message, $headers);
    }
}

function getEmailHeader() {
    $headerHTML = '
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="" content="" />
            <meta name="" content="" />
            <style type="text/css">
                body {
                    margin:0px;
                    padding:0px;
                    color:#333333;
                    font-family:Arial, Helvetica, sans-serif;
                    font-size:14px;
                    width:100% !important;
                }
            </style>
        </head>
        <body>
        <table width="99%" cellspacing="0" cellpadding="0" border="0">
            <!-- Start of main container -->
            <tr><td><table width="100%" cellspacing="0" cellpadding="0">';
    return $headerHTML;
}

function getEmailFooter() {
        $footerHTML = '<tr><td height="25" align="left" colspan="2" valign="middle" bgcolor="#FFFFFF">&nbsp;</td></tr>
        <tr><td height="25" colspan="2" align="left" valign="middle" bgcolor="#FFFFFF">Thank you,<br/><a href="' . site_url() . '">'.get_option( 'blogname' ).'</a></td></tr>
        
            </table>
            <!-- End of footer -->
            </td>
            </tr>
            <!-- End of main container -->
            </table>
            </body>
            </html>'; 
    return $footerHTML;
}
if ( ! function_exists('write_log')) {
    function write_log ( $log )  {
       if ( is_array( $log ) || is_object( $log ) ) {
          error_log( print_r( $log, true ) );
       } else {
          error_log( $log );
       }
    }
}
?>