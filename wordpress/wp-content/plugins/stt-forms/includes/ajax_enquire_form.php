<?php
function ajax_enquire_form(){
// Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $first_name = strip_tags(trim($_POST["first_name"]));
				$first_name = str_replace(array("\r","\n"),array(" "," "),$first_name);
        $last_name = strip_tags(trim($_POST["last_name"]));
				$last_name = str_replace(array("\r","\n"),array(" "," "),$last_name);
        $phone = strip_tags(trim($_POST["phone"]));
				$phone = str_replace(array("\r","\n"),array(" "," "),$phone);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $message = trim($_POST["msg"]);

        //debug_to_console($first_name);
        //debug_to_console($email);
        //debug_to_console($message);
        // Check that data was sent to the mailer.
        if ( empty($first_name) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            //http_response_code(400);
            //echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            //exit;
            $response['status'] = false;
        }

        if (1) {
            // Set a 200 (okay) response code.
            //http_response_code(200);
            $response['msg'] =  "Thank you ".$first_name." ".$last_name.".  Your message has been sent.";
            $response['status'] = true;
            
            
            if(is_plugin_active('ot_infusionsoft/ot_infusionsoft.php')){
                debug_to_console('writing to infusionsoft');
            }else if(is_plugin_active('customers_db/customers_db.php')){
                $newCust = new Cust_Record;
                $newCust->first_name = $first_name;
                $newCust->last_name = $last_name;
                $newCust->phone = $phone;
                $newCust->email = $email;
                $newCust->message = $message;
                $newCust->saveToDb();
                $newCust->email_vendor();
            }
            
        } else {
            // Set a 500 (internal server error) response code.
            //http_response_code(500);
            $response['msg'] = "Oops! Something went wrong and we couldn't send your message.";
            $response['status'] = false;
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        //http_response_code(403);
        $response['msg'] = "There was a problem with your submission, please try again.";
        $response['status'] = false;
    }

    wp_send_json($response);
    exit;
}
?>