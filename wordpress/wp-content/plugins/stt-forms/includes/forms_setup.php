<?php
function forms_setup() { 
    global $wpdb;
    $table_name = $wpdb->prefix.STT_FORMS_TABLE;

    //create table if doesn't exist
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
      debug_to_console($table_name);
      create_settings_table(STT_FORMS_TABLE);
    }
    if (isset($_REQUEST['setting'])) {
        if ($_REQUEST['setting'] == 1) {
            $settingArray = array(
                'to_email' => $wpdb->_real_escape($_POST['to_email']),
                'from_email' => $wpdb->_real_escape($_POST['from_email']),
                'cc_email' => $wpdb->_real_escape($_POST['cc_email']),
                'bcc_email' => $wpdb->_real_escape($_POST['bcc_email'])
            );
            $settingsDataJSON = json_encode($settingArray);
            $result = $wpdb->query("UPDATE `" . $table_name . "` SET `settings_data` = '" . $settingsDataJSON . "' WHERE `id` =1");
            if($result == 1){
                $successMessage = "Settings updated successfully.";
            }
        }
    }
    
    ?>

  <div >
    <h1>Seventeen Tech Forms Settings</h1>
    <?php if(isset($successMessage)): ?>
    <h2><?php echo $successMessage; unset($successMessage); ?> </h2>
    <?php endif; ?>
    <div >
        <?php
                $data = array(
                              'to_email' =>'REQ',
                              'from_email' =>'REQ',
                              'cc_email' =>'REQ',
                              'bcc_email' => 'REQ');
                $dataSettings = $wpdb->get_results("SELECT * FROM " . $table_name . " where `id` =1");
                $dataJSON = $dataSettings[0]->settings_data;
                if (strlen(trim($dataJSON)) > 0) {
                    $data = json_decode($dataJSON, true);
                } //else {
                   // $data = array('authnet_test_mode' => 0, 'authnet_api_login' => '', 'authnet_api_key' => '', 'tran_result_page' =>'required');
                //}
            ?>
      <form action="<?php echo site_url(); ?>/wp-admin/admin.php?page=forms_setup" method="post" name="setting" id="setting">
        <input type="hidden" name="setting" value="1" />
        <table cellpadding="0" cellspacing="0" bgcolor="#ccc" width="99%" class="options_headers">
          <tr>
            <td><table cellpadding="10" cellspacing="1" width="100%" >
              <tr>
                <td bgcolor="#FFFFFF"><label for="admin_email"><strong>To Email</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['to_email']; ?>" id="to_email" name="to_email"></td>
              </tr>
              <!--
              <tr>
                <td bgcolor="#FFFFFF"><label for="admin_email"><strong>From Email</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['from_email']; ?>" id="from_email" name="from_email"></td>
              </tr>
              -->
              <tr>
                <td bgcolor="#FFFFFF"><label for="manager_email"><strong>CC Email</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['cc_email']; ?>" id="cc_email" name="cc_email"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><label for="general_email"><strong>BCC Email</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['bcc_email']; ?>" id="bcc_email" name="bcc_email"></td>
              </tr>
              <tr>
                <td colspan="2" align="left" bgcolor="#FFFFFF"><input type="submit" value="Submit"  class="button button-primary button-large" /></td>
              </tr>
            </table></td>
          </tr>
        </table>
      </form>
    </div>
    <div style="margin:5px">
      
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="" content="" />
            <meta name="" content="" />
            <style type="text/css">
                body {
                    margin:0px;
                    padding:0px;
                    color:#333333;
                    font-family:Arial, Helvetica, sans-serif;
                    font-size:14px;
                    width:100% !important;
                }
            </style>
        </head>
        <body>
          <div style="margin:5px">
            <div style="margin-top:10px; background: #FFFFFF">
                SUBJECT: New Enquiry
            </div>
            <table width="99%" cellspacing="0" cellpadding="0" border="0">
                <!-- Start of main container -->
                <tr>
                  <td>
                    <table width="100%" cellspacing="0" cellpadding="0">
                      <div style="margin-left:20px; margin-right:20px">
                        <table width="100%" cellspacing="1" cellpadding="10">
                                <tr><td height="25" align="left" valign="middle" bgcolor="#FFFFFF">
                                    <p style="margin-bottom:2px">Name: FirtName LastName</p>
                                    <p style="margin-bottom:2px">email: customerEmail</p>
                                    <p style="margin-bottom:2px">phone: 555-555-5555</p>
                                </td></tr>
                        </table>
                        <div style="background: #FFFFFF">
                            Customer Message: 
                            <p style="margin:10px"> Want for info... </p>
                            www.marinecharters.com
                        </div>
                      </div>
                    </table>
            <!-- End of footer -->
                  </td>
                </tr>
            <!-- End of main container -->
            </table>
          </div>
        </body>
      </html>
    </div>
  </div>
  <?php 
}

function create_settings_table($table_name){
    global $wpdb;
       //table not in database. Create new table
    $table_name = $wpdb->prefix.$table_name;
    $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            settings_data text NOT NULL,
            UNIQUE KEY id (id)
    )";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $_SESSION['got_message'] .= '<div>Did not create '.$table_name.'</div>';   
    }else{
        
        $settingArray = array(
            'to_email' => '',
            'from_email' => '',
            'cc_email' => '',
            'bcc_email' => ''
        );
        $settingsDataJSON = json_encode($settingArray);
        $results = $wpdb->insert( $table_name, array( 
            'settings_data' => $settingsDataJSON),
            array('%s'));
        if($result == 1){
            $successMessage = "Settings updated successfully.";
        }
    }
  } ?>