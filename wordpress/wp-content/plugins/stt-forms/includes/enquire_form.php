<?php
function enquire_form() {
  $formName = "CONTACT US";
  global $wpdb;
  $table_name = $wpdb->prefix.STT_FORMS_TABLE;
  if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
    $formName = "CONTACT US";
  }
  //need to make sure nothing is sent too early
  // without return ob_get_clean, we will get header error.
  ob_start();
?>

<div class="center-item">
  <h3><?php echo $formName ?> </h3>
</div>
<div id="enquire_sent_status"></div>
<form id="ajax_enquire" name="enquire_msg" method="post" action="<?php echo admin_url('admin-ajax.php'); ?>">
 <div class="center-item">
  <section>
    <div style="width:100%">
      <div style="float:left;margin-right:20px; min-width:240px">
        <label for="first_name">First Name *</label>
        <input id="first_name" style="width: 100%; max-width: 250px" type="text" value="" name="first_name" required>
      </div>
      <div style="float:left;margin-right:0px; min-width:240px">
        <label for="last_name">Last Name *</label>
        <input id="last_name" style="width: 100%; max-width: 250px" type="text" value="" name="last_name" required>
      </div>
      <br style="clear:both;" />
    </div>
    <div style="width: 100%">
      <div style="float:left;margin-right:20px; min-width:240px">
        <label for="email">Email *</label>
        <input id="email" style="width: 100%; max-width: 250px" type="email" value="" name="email" required>
      </div>
      <div style="float:left; min-width:240px">
        <label for="phone">Phone Number</label>
        <input id="phone" style="width: 100%; max-width: 250px" type="text" value="" name="phone">
      </div>
      <br style="clear:both;" />
    </div>
  </section>
  <section>
    <div>
      <label for="message">Message</label>
      <textarea style="width: 100%; max-width: 500px" rows="4" cols="50" name="msg"> </textarea>
    </div>
  </section>
  
  <!--  This is where the submit button start -->
  <section>
    <div>
      <input type="hidden" name="action" value="ajax_enquire_form">
    </div>
    <div id="submit_button">
      <input style="margin-top: 5px"  type="submit" name="submit_enquire_form" id="submit_enquire_form" value="SUBMIT">
    </div>
    <div id="showSending" style="display:none">
      <input style="margin-top: 5px"  type="button" name="button_enquire_form" id="submit_enquire_form" value="WAIT...">
    </div>
  </section>
  <!--  This is where the submit button ends -->
  </div>
 </div>
</form>
<div id="calButton" style="margin-bottom:20px"><button id="calBut" type="button">calendar</button></div>
<div id="showCalendar" style="display:none">
  <!-- 
  <iframe src="https://app.acuityscheduling.com/schedule.php?owner=19495581" title="Schedule Appointment" width="100%" height="800" frameBorder="0"></iframe>
  -->
  <iframe src="https://app.acuityscheduling.com/schedule.php?owner=19488126" title="Schedule Appointment" width="100%" height="800" frameBorder="0"></iframe>
  <script src="https://embed.acuityscheduling.com/js/embed.js" type="text/javascript"></script>
</div>
  <script>
    document.addEventListener("DOMContentLoaded", function () {
      jQuery("#calBut").click(function(){
        jQuery("#calButton").hide();
        jQuery('#showCalendar').show();
        console.log('we are here');
      });
    });
  </script>
	<script>
    document.addEventListener("DOMContentLoaded", function () {
      // Get the form.
      var form = jQuery('#ajax_enquire');

      // Get the messages div.
      var formMessages = jQuery('#enquire_form_status');

      // TO/DO: The rest of the code will go here...
      jQuery(form).submit(function(event) {
        // Stop the browser from submitting the form.
        event.preventDefault();
        var formData = jQuery(form).serialize();
        console.log(formData);
        var url_link = jQuery(form).attr('action');
        console.log(url_link);
        // Submit the form using AJAX.
        jQuery('#showSending').show();
        jQuery('#submit_button').hide();
        jQuery.ajax({
          type: 'POST',
          url: url_link,
          data: formData,
          cache: false,
          processData: false,
          timeout: 15000,
          async: true,
          headers: {
              "cache-control": "no-cache"
            },
          }).done(function(response) {
          // Make sure that the formMessages div has the 'success' class.
            jQuery(enquire_sent_status).removeClass('error');
            jQuery(enquire_sent_status).addClass('success');
            // Set the message text.
            jQuery(enquire_sent_status).text(response['msg']);
            // Clear the form.
            jQuery('#ajax_enquire').hide();
            jQuery('#first_name').val('');
            jQuery('#email').val('');
            jQuery('#msg').val('');
          }).fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            jQuery(enquire_sent_status).removeClass('success');
            jQuery(enquire_sent_status).addClass('error');

            console.log(data);
            jQuery('#ajax_enquire').hide();
            // Set the message text.
            if (data.responseText !== '') {
              jQuery(enquire_sent_status).text(data.responseText);
            } else {
              jQuery(enquire_sent_status).text('Oops! An error occured and your message could not be sent.');
            }
          });
      });
      // TODO
  });
  </script>
<?php
  return ob_get_clean();
}
?>