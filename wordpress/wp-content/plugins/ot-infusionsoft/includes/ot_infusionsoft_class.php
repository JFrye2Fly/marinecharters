<?php

/*
 *
 * @package    WordPress
 * @subpackage OT Infusionsoft Plugin
 */
if ( !defined( 'ABSPATH' ) )
  die;
  
class Test1 extends \Infusionsoft\Infusionsoft{

}

class OTInfusionsoft extends \Infusionsoft\Infusionsoft{
    //public $infusionsoft;
    public function __construct(){
       
        //$infusionsoft = new \Infusionsoft\Infusionsoft($this->getInfusionsoftDbInfo());
        parent::__construct($this->getInfusionsoftDbInfo());
        $this->checkParentTokens();
    }
    function getInfusionsoftDbInfo(){
        $data = get_infusionsoft_settings();
        $dbInfo = null;
        if($data && $data['app_key']){
          $dbInfo = (array(
              'clientId'     => $data['app_key'],
              'clientSecret' => $data['app_secret'],
              'redirectUri'  => $data['redirect_uri'],
          ));
        }
        return $dbInfo;
    }
    function checkParentTokens(){
        // If the serialized token is available in the session storage, we tell the SDK
        // to use that token for subsequent requests.
        if (isset($_SESSION['infusionsoft_token'])) {
            parent::setToken(unserialize($_SESSION['infusionsoft_token']));
            $token = parent::getToken();
            if($token->isExpired()){
                parent::refreshAccessToken();
                $_SESSION['infusionsoft_token'] = serialize(parent::getToken());
            }
        } 
    }
}
?>