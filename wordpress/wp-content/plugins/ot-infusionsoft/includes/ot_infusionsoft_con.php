<?php
function ot_infusionsoft_con() {
   
    /*
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      
            $response['msg'] =  "Thank you ".$first_name." ".$last_name.".  Your message has been sent.";
            $response['status'] = true;
            
            
            if(is_plugin_active('customers_db/customers_db.php')){
               debug_to_console('active');
            }
            
       

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        //http_response_code(403);
        $response['msg'] = "There was a problem with your submission, please try again.";
        $response['status'] = false;
    }

    wp_send_json($response);
    */
    if(empty(session_id())) 
        session_start();

    
    $credentials = get_infusionsoft_settings();
    $infusionsoft = new \Infusionsoft\Infusionsoft(array(
	    'clientId'     => $credentials['app_key'],
	    'clientSecret' => $credentials['app_secret'],
	    'redirectUri'  => $credentials['redirect_uri'],
    ));

    // If the serialized token is available in the session storage, we tell the SDK
    // to use that token for subsequent requests.
    if (isset($_SESSION['token'])) {
	    $infusionsoft->setToken(unserialize($_SESSION['token']));
        //debug_to_console($_SESSION['token']);
    }

    // If we are returning from Infusionsoft we need to exchange the code for an
    // access token.
    if (isset($_GET['code']) and !$infusionsoft->getToken()) {
        $_SESSION['token'] = serialize($infusionsoft->requestAccessToken($_GET['code']));
        //debug_to_console('!getToken');
        //debug_to_console($_SESSION['token']);
        //return $_SESSION['token'];
    }

    if ($infusionsoft->getToken()) {
	    // Save the serialized token to the current session for subsequent requests
    	$_SESSION['token'] = serialize($infusionsoft->getToken());
        //debug_to_console($_SESSION['token']);
        //debug_to_console('getToken');
        //return $_SESSION['token'];
	    // MAKE INFUSIONSOFT REQUEST
    } else {
        $data = (object) [
            'result' => 'success',
            'url' => '',
          ];
        $data->url = $infusionsoft->getAuthorizationUrl();
        //debug_to_console('else');
        //debug_to_console($data);
        //echo '<a href="' . $infusionsoft->getAuthorizationUrl() . '">Click here to authorize</a>';
        
        //return $data;
        //header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }
}

?>