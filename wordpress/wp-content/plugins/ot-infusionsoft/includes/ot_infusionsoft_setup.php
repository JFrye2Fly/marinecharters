<?php
function ot_infusionsoft_setup() { 
    global $wpdb;
    $table_name = $wpdb->prefix.OT_INFUSIONSOFT_TABLE;

    //create table if doesn't exist
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
      create_ot_infusionsoft_settings_table(OT_INFUSIONSOFT_TABLE);
    }
    if (isset($_REQUEST['setting'])) {
        if ($_REQUEST['setting'] == 1) {
            $settingArray = array(
                'email' => $wpdb->_real_escape($_POST['email']),
                'app_name' => $wpdb->_real_escape($_POST['app_name']),
                'app_key' => $wpdb->_real_escape($_POST['app_key']),
                'app_secret' => $wpdb->_real_escape($_POST['app_secret']),
                'base_url' => $wpdb->_real_escape($_POST['base_url']),
                'redirect_uri' => $wpdb->_real_escape($_POST['redirect_uri']),
            );
            $settingsDataJSON = json_encode($settingArray);
            $result = $wpdb->query("UPDATE `" . $table_name . "` SET `settings_data` = '" . $settingsDataJSON . "' WHERE `id` =1");
            if($result == 1){
                $successMessage = "Settings updated successfully.";
            }
        }
    }
    $connectButton = 'Refresh Connection';

    if(empty(session_id())) 
        session_start();

    $data = get_infusionsoft_settings();
    if($data && $data['app_key']){
      $infusionsoft = new \Infusionsoft\Infusionsoft(array(
  	    'clientId'     => $data['app_key'],
  	    'clientSecret' => $data['app_secret'],
  	    'redirectUri'  => $data['redirect_uri'],
      ));

      // If the serialized token is available in the session storage, we tell the SDK
      // to use that token for subsequent requests.
      if (isset($_SESSION['infusionsoft_token'])) {
	      $infusionsoft->setToken(unserialize($_SESSION['infusionsoft_token']));
      }
      // If we are returning from Infusionsoft we need to exchange the code for an
      // access token.
      if (isset($_GET['code'])) {
        $_SESSION['infusionsoft_token'] = serialize($infusionsoft->requestAccessToken($_GET['code']));
        saveToInfusionsoftDb('token', $_SESSION['infusionsoft_token']);
      }

      if ($infusionsoft->getToken()) {
	      // Save the serialized token to the current session for subsequent requests
      	$_SESSION['infusionsoft_token'] = serialize($infusionsoft->getToken());
	      // MAKE INFUSIONSOFT REQUEST
      } else {
          $connectButton = 'Connect To InfusionSoft';
      }
      $auth_url = $infusionsoft->getAuthorizationUrl();
    }
    if(isset($_SESSION['infusionsoft_token'])){
      $connection_good = true; 
    }
    ?>

  <div >
    <h1>OT Infusionsoft Settings</h1>
    <?php if(isset($successMessage)): ?>
    <h2><?php echo $successMessage; unset($successMessage); ?> </h2>
    <?php endif; ?>
    <div >
      <form action="<?php echo site_url(); ?>/wp-admin/admin.php?page=ot_infusionsoft_setup" method="post" name="setting" id="setting">
        <input type="hidden" name="setting" value="1" />
        <table cellpadding="0" cellspacing="0" bgcolor="#ccc" width="99%" class="options_headers">
          <tr>
            <td><table cellpadding="10" cellspacing="1" width="100%" >
              <tr>
                <td bgcolor="#FFFFFF"><label for="admin_email"><strong>Email</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['email']; ?>" id="email" name="email"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><label for="manager_email"><strong>App Name</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['app_name']; ?>" id="app_name" name="app_name"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><label for="general_email"><strong>App Key</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['app_key']; ?>" id="app_key" name="app_key"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><label for="general_email"><strong>App Secret</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['app_secret']; ?>" id="app_secret" name="app_secret"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><label for="general_email"><strong>Base URL</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['base_url']; ?>" id="base_url" name="base_url"></td>
              </tr>
              <tr>
                <td bgcolor="#FFFFFF"><label for="general_email"><strong>Redirect URI</strong></label></td>
                <td width="80%" bgcolor="#FFFFFF"><input type="text" style="width:100%" value="<?php echo $data['redirect_uri']; ?>" id="redirect_uri" name="redirect_uri"></td>
              </tr>
              <tr>
                <td colspan="2" align="left" bgcolor="#FFFFFF"><input type="submit" value="Submit"  class="button button-primary button-large" /></td>
              </tr>
            </table></td>
          </tr>
        </table>
      </form>
    </div>
    <?php
      if ($connection_good) {
    ?>
    <div style="margin:40px">
    <hr>
      <div><span>Connection to Infusionsoft - <i style="color:green">connected </i></span></div>
    </hr>
    </div>
    <?php
      }else if($auth_url){
    ?>
      <div> No connection to InfusionSoft was found.  Click on the botton below to connect!</div>
    <?php
      }
    ?>
  </div>
  <?php 
  if($auth_url){
    ?>
      <div style="margin:40px">
        <button class="boldOnHover" id="oathButton" type="button"><?php echo $connectButton ?></button>
        <button class="boldOnHover" style="display:none" id="oathButtonWorking" type="button">Getting Link ...</button>
        <button class="boldOnHover" style="display:none" id="errorButton" type="button">Error Getting Link</button>
      </div>
      <script>
        document.addEventListener("DOMContentLoaded", function () {
          jQuery("#oathButton").click(function(){
            jQuery("#oathButton").hide();
            jQuery('#oathButtonWorking').show();
            url_link = "<?php echo $auth_url ?>";
            //window.open( url_link, "_blank");
            window.open( url_link, "_self");
          });
        });
      </script>
    <?php
  }
}
function get_infusionsoft_settings(){
    unset($_SESSION['infusionsoft_token']);
    global $wpdb;
    $table_name = $wpdb->prefix.OT_INFUSIONSOFT_TABLE;
    $data = array(
        'email' =>null,
        'app_name' =>null,
        'app_key' =>null,
        'app_secret' =>null,
        'base_url' => null,
        'redirect_uri' => null,
        'authorization_url' =>null,
        'auth_token'=>null,
    );
    $dataSettings = $wpdb->get_results("SELECT * FROM " . $table_name . " where `id` =1");
    $settingsDataJSON = $dataSettings[0]->settings_data;
    if (strlen(trim($settingsDataJSON)) > 0) {
      $data = json_decode($settingsDataJSON, true);
    }
    if($dataSettings[0]->token){
      $_SESSION['infusionsoft_token'] = $dataSettings[0]->token;
    }else{
      if(isset($_SESSION['infusionsoft_token']))
        saveToInfusionsoftDb('token', $_SESSION['infusionsoft_token']);
      else 
        unset($_SESSION['infusionsoft_token']);
    }
    return $data;
}

function create_ot_infusionsoft_settings_table($table_name){
    global $wpdb;
       //table not in database. Create new table
    $table_name = $wpdb->prefix.$table_name;
    $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            settings_data text NOT NULL,
            token text,
            UNIQUE KEY id (id)
    )";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $_SESSION['got_message'] .= '<div>Did not create '.$table_name.'</div>';   
    }else{
        
        $settingArray = array(
            'email' => '',
            'app_name' => '',
            'app_key' => '',
            'app_secret' => '',
            'base_url' => '',
            'redirect_uri' => '',
            'authorization_url' =>'',
            'auth_token'=>'',
        );
        $settingsDataJSON = json_encode($settingArray);
        $tokenDataJSON = json_encode($tokenArray);
        $results = $wpdb->insert( $table_name, array( 
            'settings_data' => $settingsDataJSON,
            'token' => null),
            array('%s', '%s'));
        if($result == 1){
            $successMessage = "Settings updated successfully.";
        }
    }
    
}

function saveToInfusionsoftDb($field_name, $value){
    global $wpdb;
    $table_name = $wpdb->prefix.OT_INFUSIONSOFT_TABLE;
    $result = ($wpdb->query($wpdb->prepare("UPDATE $table_name SET $field_name=%s WHERE id=%d", $value, 1)));
}

?>