<?php
/**
 * Plugin Name: OT Infusionsoft
 * Plugin URI: https://olteantech.com
 * Description: Plugin for managing a database of customers
 * Author: Dinu Oltean
 * Version: 1.0
 * Author URI: https://olteantech.com
 * Text Domain: ot-infusionsoft
 * Domain Path: /languages
 */
/*
 * 
 */
if ( !defined( 'ABSPATH' ) )
  exit;
@define('OT_INFUSIONSOFT', WP_PLUGIN_URL . '/' . end(explode(DIRECTORY_SEPARATOR, dirname(__FILE__))));
@define('OT_INFUSIONSOFT_TABLE', 'ot_infusionsoft_settings');

if ( ! function_exists( 'is_plugin_active' ) )
    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

//register_forms_scripts();
//include_once('includes/enquire_form.php');
//include_once('includes/ajax_enquire_form.php');
require_once 'vendor/autoload.php';
include_once('includes/ot_infusionsoft_top.php');
include_once('includes/ot_infusionsoft_setup.php');
include_once('includes/ot_infusionsoft_con.php');
include_once('includes/ot_infusionsoft_class.php');
//include_once('includes/email_stuff.php');



//for AJAX calls
add_action('wp_ajax_ot_infusionsoft_con', 'ot_infusionsoft_con');  //logged in users
add_action('wp_ajax_nopriv_ot_infusionsoft_con', 'ot_infusionsoft_con');  //not logged in users

//add_action( ‘plugins_loaded’, array( $this, 'enqueue_forms_scripts' ) );
add_action('wp_print_styles', 'enqueue_ot_infusionsoft_styles');
//setup AJAX so we can stay on the same page
//add_action( 'wp_ajax_ajax_enquire_form', 'ajax_enquire_form' );
//add_action( 'wp_ajax_nopriv_ajax_enquire_form', 'ajax_enquire_form' );
//Shortcodes to pull in the form into any page
register_activation_hook(__FILE__, 'install_ot_infusionsoft_table');
add_shortcode('infusionsoft_connect_test', 'ot_infusionsoft_top');
add_action('admin_menu', 'manage_ot_infusionsoft');

function install_ot_infusionsoft_table(){
    create_ot_infusionsoft_settings_table(OT_INFUSIONSOFT_TABLE);
}
function manage_ot_infusionsoft() {
    add_menu_page('OT Infusionsoft', 'OT Infusionsoft', 'manage_options', 'ot_infusionsoft_setup');
    add_submenu_page('ot_infusionsoft_setup', 'Settings', 'Settings', 'manage_options', 'ot_infusionsoft_setup', 'ot_infusionsoft_setup');
}

/**
 * Enqueues plugin-specific styles.
 */
function enqueue_ot_infusionsoft_styles() {   
    
    wp_enqueue_style('ot_infusionsoft', OT_INFUSIONSOFT . '/css/ot_infusionsoft.css', '', rand(111,9999));    
    //wp_enqueue_style( 'forms-style' ); 
}
//spl_autoload_register( 'CDb_include_files' );