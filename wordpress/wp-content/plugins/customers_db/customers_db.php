<?php
/**
 * Plugin Name: Customers DB
 * Plugin URI: https://seventeentech.com
 * Description: Plugin for managing a database of customers
 * Author: Dinu Oltean
 * Version: 1.0
 * Author URI: https://seventeentech.com
 * Text Domain: customers-db
 * Domain Path: /languages
 */
/*
 * 
 */
if ( !defined( 'ABSPATH' ) )
  exit;
// register the class autoloading
spl_autoload_register( 'CDb_class_loader' );
@define('CUSTOMER_DB_PATH', WP_PLUGIN_URL . '/' . end(explode(DIRECTORY_SEPARATOR, dirname(__FILE__))));

//spl_autoload_register( 'CDb_include_files' );
include_once('includes/store_customer.php');
include_once('includes/customer_list.php');

add_action('wp_enqueue_scripts', 'enqueue_db_styles');
register_activation_hook(__FILE__, 'install_wp_customer_db');

add_action('admin_menu', 'manage_add_menu_items');

function manage_add_menu_items() {
    add_menu_page('Customer DB', 'Customer DB', 'manage_options', 'customer_db');
    add_submenu_page('customer_db', 'Settings', 'Settings', 'manage_options', 'customer_db', 'customer_db');
    wp_enqueue_style('customers_db_css', CUSTOMER_DB_PATH . '/css/customers_db.css', '', rand(111,9999));    
}
// this is not firing so added above
function enqueue_db_styles() {   
    wp_enqueue_style('customers_db_css', CUSTOMER_DB_PATH . '/css/customers_db.css', '', rand(111,9999));    
}
function install_wp_customer_db(){
    create_db_table();
}

/**
 * main static class for running the plugin
 * 
 * @category   WordPress Plugins
 * @package    wordPress
 * @author     Dinu Oltean
 * @copyright  2020
 * @version    Release: 1.0
 * 
 */
function CDb_class_loader( $class )
{

  if ( !class_exists( $class ) ) {
    
    $file = ltrim(str_replace('\\', '/', $class), '/') . '.php';

    /**
     * allows class overrides
     * 
     * @filter cdb-autoloader_class_path
     * @param string the absolute path to the class file
     * @return string
     */
    $class_file = apply_filters( 'cdb-autoloader_class_path', plugin_dir_path( __FILE__ ) . 'classes/' . $file ); // $class . '.class.php'
        
    if ( is_file( $class_file ) ) {

      require_once $class_file;
    }
  }
}

function create_db_table(){
    global $wpdb;
    $table_name = $wpdb->prefix.'customers_db';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name)
       //table not in database. Create new table
   
    $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            firstname VARCHAR(30) NOT NULL,
            lastname VARCHAR(30) NOT NULL,
            email VARCHAR(50) NOT NULL,
            phone VARCHAR(30),
            message_data text,
            entry_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            email_vendor TIMESTAMP,
            email_customer TIMESTAMP, 
            UNIQUE KEY id (id)
    )";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name){
        $_SESSION['got_message'] .= '<div>Did not create '.$table_name.'</div>';   
    }
}