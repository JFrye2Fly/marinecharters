<?php
/**
 * handles the presentation of the editable frontend record
 * 
 *  * 
 * @package    WordPress
 * @subpackage Customers Database Plugin
 * @author     Dinu Oltean
 * @license    GPL2
 * @version    1.0
 * 
 */
if ( ! defined( 'ABSPATH' ) ) die;
/*
 *
 */
class Cust_Record {
    public $fist_name; 
    public $last_name;
    public $email; 
    public $phone; 
    public $message;
    private $entry_date;
    private $id; 
    private $emailed_vendor = 0;
    private $emailed_customer = 0;
      
    function __construct() {
       
    } 
    function load($inCust) {
      if($inCust){
          $this->first_name = $inCust->firstname;
          $this->last_name = $inCust->lastname;
          $this->phone = $inCust->phone;
          $this->email = $inCust->email;
          $this->message = $inCust->message_data;
          $this->id = $inCust->id;
          $this->entry_date = $inCust->entry_date;
          $this->email_vendor = $inCust->email_vendor;
          $this->email_customer = $inCust->email_customer;
      }
    } 
    public function saveToDb(){
        global $wpdb;
        $table = $wpdb->prefix . 'customers_db';
        $email = $this->email;
        $search = $wpdb->get_results("SELECT * FROM $table WHERE email = '$email' ");
        // only save if email not in DB
        if(!(count($search) > 0)){
            $results = $wpdb->insert( $table, 
              array( 
                'firstname' => $this->first_name,
                'lastname' => $this->last_name,
                'email' => $this->email,
                'phone' => $this->phone,
                'message_data' => $this->message,
              ),
              array('%s','%s','%s','%s','%s')
            );
        }
        $info = $email." ".count($search)." ...inserted = ".$result;
        return $info;
    }
    public function email_vendor(){
        // get email header
        $message = "";
        $message .= getEmailHeader();
        $subject = "New Enquiry";
        $message .='<div style="margin-left:20px; margin-right:20px">';
        $message .= "
          <tr><td height='50' colspan='2' align='left' valign='middle' bgcolor='#FFFFFF'>
            Customer: <strong>".  ucfirst($this->first_name)." ".ucfirst($this->last_name)."</strong>
          </td></tr>";
        $message .= '
          <tr><td colspan="2" bgcolor="#ccc"><table width="100%" cellspacing="1" cellpadding="10">
          <tr><td height="25" align="left" valign="middle" bgcolor="#FFFFFF">
          <p style="margin-bottom:2px">email: '.$this->email.'</p>
          <p style="margin-bottom:2px">phone: '.$this->phone.'</p></td></tr>
              </table></td></tr>';
        $message .="
            <tr><td height='25' colspan='2' align='left' valign='middle' bgcolor='#FFFFFF'>"
            .$this->message.
          "</td></tr>
          ";
        
        $message .='</div>';
        // get email footer
        $message .= getEmailFooter(); 
    
        sendEmail($subject, $message);
    }
    
}