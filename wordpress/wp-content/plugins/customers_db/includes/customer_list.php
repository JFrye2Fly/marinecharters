<?php
function customer_db() { 
    global $wpdb;
    $table_name = $wpdb->prefix . 'customers_db';
    $delete_id = null;
    if(isset($_POST['deleteCustId'])){
        $delete_id = $_POST['deleteCustId']; 
        $wpdb->delete( $table_name, array( 'id' => $delete_id ) );
    }
    //create table if doesn't exist
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name)
      create_db_table($table_name);
    $list = $wpdb->get_results("SELECT * FROM $table_name");
    $list_count = count($list);
    //if(!$list_count){
    //    echo "<div>No customer found.</div>";
    //    return false;
    //}
    ?>
    <h1>Customer Database</h1>
    <table style="width:90%; margin-left:auto; margin-right:auto">
        <caption >Total Records Found: <?php echo $list_count ?> </caption>    
          <thead width="50%">
            <tr style="background:lightgrey">
              <th style="width:50px"></th>
              <th >First Name</th>
              <th >Last Name</th>
              <th >Email</th>       
              <th >Phone Number</th>
              <th >Date Created</th>
              <th >Vendor Emailed</th>
              <th >Customer Emailed</th>
              <th style="max-width: 500px">Message</th>
            </tr>
          </thead>
        <tbody>
      <?php
        $i=0;
        foreach($list as $customer){
            $id = $customer->id;
            $name = $customer->firstname ." ".$customer->lastname;
      ?>
          <tr>
            <td >
                <button class="boldOnHover normButton" onclick="checkItOut('<?php echo $name ?>','<?php echo $id ?>' )">delete</button>
            </td>
            <td ><?php echo $customer->firstname ?> </td>
            <td ><?php echo $customer->lastname ?> </td>
            <td ><?php echo $customer->email ?> </td>
            <td ><?php echo $customer->phone ?> </td>
            <td ><?php echo $customer->entry_date ?> </td>
            <td ><?php echo $customer->email_vendor ?> </td>
            <td ><?php echo $customer->email_customer ?> </td>
            <td ><?php echo $customer->message_data ?> </td>
          </tr>
      <?php
        }
      ?>
        </tbody>
      </table>
      <form id="deleteCust" name="deleteCust" method="post">
        <input id="deleteCustId" type="hidden" name="deleteCustId" value="">
      </form>
      <script>
        var txt;
        function checkItOut(name, id){
            console.log(name);
            let msg = "Delete "+name;
            if(confirm(msg)){
                txt = "yes";
                var form = jQuery('#deleteCust');
                jQuery('#deleteCustId').val(id);
                console.log(jQuery('#deleteCustId').val());
                form.submit();
                //post(null, {deleteId: id});
            }else{
                txt = "no";
            }
        }
        function post(path, parameters) {
            var form = $('<form></form>');

            form.attr("method", "post");
            form.attr("action", path);
            $.each(parameters, function(key, value) {
                var field = $('<input></input>');

                field.attr("type", "hidden");
                field.attr("name", key);
                field.attr("value", value);

                form.append(field);
            });
            // The form needs to be a part of the document in
            // order for us to be able to submit it.
            $(document.body).append(form);
            form.submit();
        }
      </script>
  <?php
}
?>