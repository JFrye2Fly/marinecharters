<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'marinecharter' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define('DB_HOST','localhost');

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'cv>75~(JGR9N>]AwrKzsW/XfM/|6XnNF9/z M@bZJ#Tg5@`K3f@^l^|epnBk3[]n' );
define( 'SECURE_AUTH_KEY',  'OMLQA`}}%LDn|]Tc`kqDkc8U-&,AI6[M-|~F:%MYI H=0]<c6vvYxQiQ500Ig_C~' );
define( 'LOGGED_IN_KEY',    'IrCW#[RL^B(/yDheXM(#s25D8iwAJG&rI T:ISNux}FDoee;#Rfv4CbfsV(z]X(0' );
define( 'NONCE_KEY',        ';eie8FCx[6]hpB=2Ta*>YWeBUs~F52^77-VR0JE7R!OrA3hv:3ZljSwEIfWY8SZg' );
define( 'AUTH_SALT',        ' kSht-2+b9n@gPTF$R}q ib$-R!0G _Y0=y4!VH=_Y_iA;M.I]hW1#kyf;E=%* m' );
define( 'SECURE_AUTH_SALT', 'd>7JXQK;Ej7iSXfLyZMo<qhJdqUZYG,`<.*eq,R*1?~G?xm)?4kS!fx3[_PZp4ND' );
define( 'LOGGED_IN_SALT',   'U6=Q$yd!>K%!Wr8sd*.u9  a7R9/gQ8G!}2YWXkR87l7~UGzTJWCVq2GMQ;.JXWl' );
define( 'NONCE_SALT',       ' b_&1d86x|dEW6Pc/*o-Ominjfi:MrqMLjO4Co-$oa1O(NyVNf5tDBToa*Zb4BXm' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

@ini_set('log_errors','On'); // enable or disable php error logging (use 'On' or 'Off')
//@ini_set('display_errors','On'); // enable or disable public display of errors (use 'On' or 'Off')
@ini_set('display_errors','Off'); // enable or disable public display of errors (use 'On' or 'Off')
@ini_set('error_log','/Applications/MAMP/htdocs/marinecharters/wordpress/php-errors.log'); // path to server-writable log file
